#include <stdio.h>
#include <stdlib.h>
#include "database.h"

Course* parseString(FILE* fptr) {
	//TA given code that parses string
	int c1, c2;
	char division[3], title[LENGTH+1];
	int dept, num;
	int count = 0;
	int c = ' ';
	fscanf(fptr, "%2s.%d.%d %d.%d ", division, &dept, &num, &c1, &c2);
	while (count < LENGTH && (c = fgetc(fptr)) != '\n' && !feof(fptr)) {
	    title[count++] = (char) c;
	}
	title[count] = '\0';
	if(!feof(fptr)) {
		while (c != '\n') c = fgetc(fptr);   // skip to end of line
		//make new course object and put into heap. put all data in
		Course* retcrs = malloc(sizeof(Course));
		strcpy(retcrs->division, division);
		retcrs->dept = dept;
		retcrs->num = num;
		retcrs->cred[0] = c1;
		retcrs->cred[1] = c2;
		strcpy(retcrs->title, title);
		
		return retcrs;
	}
	else {
		return NULL;
	}
}

int parseShortString(FILE* fptr, int retVal[]) {
	int dept, num;
	fscanf(fptr, "%d.%d", &dept, &num);
	if(1 <= dept && dept <= 700 && 100 <= num && num <= 899) {
		retVal[0] = dept;
		retVal[1] = num;
		return 0;
	} else {
		return 1;
	}
}

void writeCourse(Course* crs, FILE* foutptr) {
	size_t index = (crs->dept * 900 + (crs->num - 100)) * (sizeof(int) * 4 + sizeof(char) * 37);	//find where newcrs should be printed
	fseek(foutptr, index, SEEK_SET);			
	fwrite(crs->division, sizeof(char), 2, foutptr);	//write the division text
	fwrite(".", sizeof(char), 1, foutptr);
	fwrite(&(crs->dept), sizeof(int), 1, foutptr);		//write the dept num
	fwrite(".", sizeof(char), 1, foutptr);
	fwrite(&(crs->num), sizeof(int), 1, foutptr);		//write the course num
	fwrite(" ", sizeof(char), 1, foutptr);
	fwrite(crs->cred, sizeof(int), 1, foutptr);	//write the cred
	fwrite(".", sizeof(char), 1, foutptr);
	fwrite(((crs->cred)+1), sizeof(int), 1, foutptr);	//write the cred
	fwrite(" ", sizeof(char), 1, foutptr);
	fwrite(crs->title, sizeof(char), 30, foutptr);	//write the title 
}

void initDatabase(FILE* fptr) {
	//init a simple course to let writeCourse do all the work
	char division[3] = "";
	char title[LENGTH+1] = "";
	Course crs = {
		.dept = 0,
		.num = 0,
		.cred = {0,0}
	};
	strcpy(crs.division, division);
	strcpy(crs.title, title);
	//repeat printing null lines 560000 times
	for(int i = 0; i < 560000; i++) {
		writeCourse(&crs, fptr);
	}
}

void courseList(FILE* finptr, FILE* foutptr) {	//main function grabs fptr from input string
	while(!feof(finptr)) {	//check if fptr has finished
		Course* newcrs = parseString(finptr);	//generate course from first line
		if(newcrs != NULL) {			//if error was caught, skip loop
			writeCourse(newcrs, foutptr);
		}
	}
}

void singleCourse(FILE* foutptr) {
	Course* newcrs = parseString(stdin);	//generate course from first line
	if(newcrs != NULL) {			//if error was caught, skip loop
		writeCourse(newcrs, foutptr);
	}
	else {
	}
}

void dispCourse(FILE* fdatptr) {
	int crsnum[2];
	if(parseShortString(stdin, crsnum) == 0) {
		size_t index = (crsnum[0] * 900 + (crsnum[1] - 100)) * (sizeof(int) * 4 + sizeof(char) * 37);	//find where newcrs should be printed
		fseek(fdatptr, index, SEEK_SET);			
	}
	else {
	}
}
