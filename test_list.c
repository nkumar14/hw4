#include "list.h"
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert(fileeq("test1.txt", "test1.txt"));
    assert(fileeq("test2.txt", "test2.txt"));
    assert(!fileeq("test2.txt", "test1.txt"));
    assert(!fileeq("test1.txt", "test2.txt"));
    assert(fileeq("test3.txt", "test3.txt"));
    assert(fileeq("test1.txt", "test3.txt"));
    assert(fileeq("test3.txt", "test1.txt"));
    assert(!fileeq("test2.txt", "test3.txt"));
    assert(!fileeq("test3.txt", "test2.txt"));
    assert(!fileeq("", ""));  // can't open file
}

Node * buildList() {
	//Build linked list, set all nexts to each other
	Node* n1 = malloc(sizeof(Node));
	Node* n2 = malloc(sizeof(Node));
	Node* n3 = malloc(sizeof(Node));
	(*n1).data = 3;
	(*n2).data = 2;
	(*n3).data = 6;
	n1->next = n2;
	n2->next = n3;
	n3->next = NULL;
	return n1;
}

void freeList(Node** headptrrf) {
	//Grab headptr of list
	Node* headptr = *headptrrf;
	Node* tempptr;
	while(headptr) {
		tempptr = headptr -> next;
		free(headptr);
		headptr = tempptr;
	}
}

void test_size() {
	//Build linked list, set all nexts to each other
	Node* n1ptr = buildList();
	Node* n2ptr = n1ptr -> next;
	Node* n3ptr = n2ptr -> next;
	
	//Assert size at each pointer is what it should be
	assert(size(n1ptr) == 3);
	assert(size(n2ptr) == 2);
	assert(size(n3ptr) == 1);
	assert(size(NULL) == 0);

	freeList(&n1ptr);
}

void testfprintList() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();

	//Defines file that shows what ouput of data at each point should be
	FILE * fexpected = fopen("fexpected.txt", "w");
	assert(fexpected);
	fprintf(fexpected, "3 2 6 ");
	fclose(fexpected);
	//Defines actual output
	FILE * fobserved = fopen("fobserved.txt", "w");
	assert(fobserved);
	fprintList(fobserved, headptr);
	fclose(fobserved);

	assert(fileeq("fexpected.txt", "fobserved.txt"));
	freeList(&headptr);
}

void checkList(char* expected_string, Node* headptr) {
	//checking function to build files to check if data are the same
	//Defines file that shows what ouput of data at each point should be
	FILE * fexpected = fopen("fexpected.txt", "w");
	assert(fexpected);
	fprintf(fexpected, "%s", expected_string);
	fclose(fexpected);
	//Defines actual output
	FILE * fobserved = fopen("fobserved.txt", "w");
	assert(fobserved);
	fprintList(fobserved, headptr);
	fclose(fobserved);

	assert(fileeq("fexpected.txt", "fobserved.txt"));
}	

void testfprintListR() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();

	//Defines file that shows what ouput of data at each point should be
	FILE * fexpected = fopen("fexpected.txt", "w");
	assert(fexpected);
	fprintf(fexpected, "3 2 6 ");
	fclose(fexpected);
	//Defines actual output
	FILE * fobserved = fopen("fobserved.txt", "w");
	assert(fobserved);
	fprintListR(fobserved, headptr);
	fclose(fobserved);

	assert(fileeq("fexpected.txt", "fobserved.txt"));
	freeList(&headptr);
}

void testfprintListRev() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();

	//Defines file that shows what ouput of data at each point should be
	FILE * fexpected = fopen("fexpected.txt", "w");
	assert(fexpected);
	fprintf(fexpected, "6 2 3 ");
	fclose(fexpected);
	//Defines actual output
	FILE * fobserved = fopen("fobserved.txt", "w");
	assert(fobserved);
	fprintListRev(fobserved, headptr);
	fclose(fobserved);

	assert(fileeq("fexpected.txt", "fobserved.txt"));
	freeList(&headptr);
}

void test_addFront() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();
	//get ** to list, pass to addFront
	Node** headptrrf = &headptr;
	addFront(headptrrf, 1);	//add node with data 1 to beginning of linked list
	//check if 1 was added to front
	checkList("1 3 2 6 ", *headptrrf);
	freeList(headptrrf);
}

void test_deleteFront() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();
	//get ** to list, pass to deleteFront
	Node** headptrrf = &headptr;
	deleteFront(headptrrf);	//delete first node
	//check deletion 
	checkList("2 6 ", *headptrrf);
	freeList(headptrrf);
}

void test_delete() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();
	//get ** to list, pass to delete
	Node** headptrrf = &headptr;
	delete(headptrrf, 2);	//delete node with 2 as data
	//check for deletion
	checkList("3 6 ", *headptrrf);
	freeList(headptrrf);
}

void test_deleteR() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();
	//get ** to list, pass to deleteR
	Node** headptrrf = &headptr;
	deleteR(headptrrf, 2);	//delete node with 2 (recursive search)
	//check for deletion
	checkList("3 6 ", *headptrrf);
	freeList(headptrrf);
}

void test_clearList() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();
	//get ** to list, pass to clearList
	Node** headptrrf = &headptr;
	clearList(headptrrf);	//clear all values
	//check for clear
	checkList("", *headptrrf);
	freeList(headptrrf);
}

void test_find() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();
	Node* correct_ptr = headptr -> next;
	Node* retptr = find(headptr, 2);	//find node
	//check if retptr is the correct one
	assert(correct_ptr == retptr);
	freeList(&headptr);
}

void test_replace() {
	//Build linked list, set all nexts to other nodes
	Node* headptr = buildList();
	//get ** to list, pass to addFront
	int retval  = replace(headptr, 2, 1);	//find replace node
	//check if retptr is the correct one
	assert(retval == 1);
	checkList("3 1 6 ", headptr);
	freeList(&headptr);
}

int main(void) {
	test_fileeq();
	test_size();
	testfprintList();
	testfprintListR();
	testfprintListRev();
	test_addFront();
	test_deleteFront();
	test_delete();
	test_deleteR();
	test_clearList();
	test_find();
	test_replace();
	return 0;
}
