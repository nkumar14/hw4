#include "list.h"
#include <stdlib.h>

int size(const Node* headptr) {
    if (headptr) {			//if node is not null
        return 1 + size(headptr->next); //return size of next node ptr in sequence
    } else {				//will keep going till hits final node and will ret 0
        return 0;			//then recursively add 1 for each node
    }
}

void fprintList(FILE* outfile, const Node* headptr) {	//prints to file for test case scenarios
    for (; headptr; headptr = headptr->next) {		//increment headptr by having it point to next node
        fprintf(outfile, "%d ", headptr->data);		//print value of each node
    }
}

void fprintListR(FILE* outfile, const Node* headptr) {	//same as above
    if (headptr) {					//however, uses recursive method of printing data
        fprintf(outfile, "%d ", headptr->data);		//prints data, and then rather than incrementing calls same 
        fprintList(outfile, headptr->next);		//function again and again
    }
}

void fprintListRev(FILE* outfile, const Node* headptr) {//same as recursive
    if (headptr) {					//however, prints backwards this time by incrementing
        fprintListRev(outfile, headptr->next);		//through nodes and then telling each to print data
        fprintf(outfile, "%d ", headptr->data);
    }
}

void printList(const Node* headptr) {
    fprintList(stdout, headptr);
}

void printListR(const Node* headptr) {
    fprintListR(stdout, headptr);
}

void printListRev(const Node* headptr) {
    fprintListRev(stdout, headptr);
}

void addFront(Node** headptrrf, int data) {
    Node* oldheadptr = *headptrrf;          //-to keep the *s straight
    Node* newheadptr = malloc(sizeof(Node));//-get a new node
    newheadptr->data = data;                //-populate with data
    newheadptr->next = oldheadptr;          //-point next to old head
    *headptrrf = newheadptr;                //-update the head by ref
}

int deleteFront(Node** headptrref) {
    Node* oldheadptr = *headptrref;	//know where old head is
    if (oldheadptr) {			//checks if old head exists
        *headptrref = oldheadptr->next;	//sets initial node* in node** list to second node
        free(oldheadptr);		//free old head
        return 1;			//return success
    } else {
        return 0;			//if no old head, say list is bad
    }
}

int delete(Node** headptrref, int todelete) {
    for (; *headptrref; headptrref = &(*headptrref)->next) {
        if ((*headptrref)->data == todelete) {
            return deleteFront(headptrref);
        }
    }
    return 0;
}

int deleteR(Node** headptrref, int todelete) {
    Node* oldheadptr = *headptrref;
    if (!oldheadptr) {                  // if list is empty, give up
        return 0;                       
    } else if (oldheadptr->data == todelete) {
        return deleteFront(headptrref); // if frst node matches, rm it
    } else {                            // otherwise, recurse
        return deleteR(&oldheadptr->next, todelete); 
    }
}

void clearList(Node** headptrref) {
    Node* oldheadptr = *headptrref;
    if (oldheadptr) {                // if the list isn't empty
        clearList(&oldheadptr->next);// clear the rest of the list
        free(oldheadptr);            // free the remaining first node
        *headptrref = NULL;          // make the headptr point to NULL
    }
}

// // Alternative version of clearList that uses deleteFront
//void clearList(Node** headptrref) {
//    while (*headptrref) {
//        deleteFront(headptrref);
//    }
//}

Node* find(Node* headptr, int tofind) {
    if (headptr == NULL || headptr->data == tofind) {
        return headptr;
    } else {
        return find(headptr->next, tofind);
    }
}

int replace(Node* headptr, int old, int new) {
    Node* toChange = find(headptr, old);
    if (toChange) {
        toChange->data = new;
        return 1;
    } else {
        return 0;
    }
}
