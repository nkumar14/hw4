#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "database.h"

/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert(fileeq("test1.txt", "test1.txt"));
    assert(fileeq("test2.txt", "test2.txt"));
    assert(!fileeq("test2.txt", "test1.txt"));
    assert(!fileeq("test1.txt", "test2.txt"));
    assert(fileeq("test3.txt", "test3.txt"));
    assert(fileeq("test1.txt", "test3.txt"));
    assert(fileeq("test3.txt", "test1.txt"));
    assert(!fileeq("test2.txt", "test3.txt"));
    assert(!fileeq("test3.txt", "test2.txt"));
    assert(!fileeq("", ""));  // can't open file
}

/*
 * Compares individual elements of course structs
 */
void crseq(Course crs1, Course crs2) {
	assert(!strcmp(crs1.division, crs2.division));	//strcmp for char arrays
	assert(crs1.dept == crs2.dept);
	assert(crs1.num == crs2.num);
	assert(crs1.cred[0] == crs2.cred[0]);
	assert(crs1.cred[1] == crs2.cred[1]);
	assert(!strcmp(crs1.title, crs2.title));	//strcmp for char arrays
}

void test_parseString() {
	FILE* fptr = fopen("test.txt", "w");
	fprintf(fptr, "EN.000.100 0.0 Swag\n");		//Write sample class
	fclose(fptr);
	fptr =  fopen("test.txt", "r");
	char ctrltitle[31] = "Swag";
	Course ctrlcrs = {				//define control course based on what
		.division = "EN",			//output should be
		.dept = 0,
		.num = 100,
		.cred = {0, 0}
	};
	strcpy(ctrlcrs.title, ctrltitle);

	Course* testcrs = parseString(fptr);		
	crseq(ctrlcrs, *testcrs);
	free(testcrs);		//avoid mem leak
	fclose(fptr);		//avoid mem leak

}

void test_parseShortString() {
	//success case
	FILE* fptr = fopen("test.txt", "w");
	fprintf(fptr, "025.125");		//sample just dep and num query
	fclose(fptr);
	fptr =  fopen("test.txt", "r");
	int testVal[2];
	assert(0 == parseShortString(fptr, testVal));
	assert(25 == testVal[0]);
	assert(125 == testVal[1]);
	fclose(fptr);

	//fail case- small num
	fptr = fopen("test.txt", "w");
	fprintf(fptr, "025.025");		//sample just dep and num query
	fclose(fptr);
	fptr =  fopen("test.txt", "r");
	assert(1 == parseShortString(fptr, testVal));
	fclose(fptr);
	
	//fail case- big num
	fptr = fopen("test.txt", "w");
	fprintf(fptr, "025.925");		//sample just dep and num query
	fclose(fptr);
	fptr =  fopen("test.txt", "r");
	assert(1 == parseShortString(fptr, testVal));
	fclose(fptr);
}

void test_writeCourse() {
	//Build expectd file
	FILE* finptr = fopen("fexpected.dat", "w+b");
	fwrite("EN.", sizeof(char), 3, finptr);		//Write sample class
	int x = 0;					//need int var because can only fwrite pointers
	char title[31] = "Swag";			//need array to define length and not overflow
	fwrite(&x, sizeof(int), 1, finptr);		
	fwrite(".", sizeof(char), 1, finptr);		
	x = 100;
	fwrite(&x, sizeof(int), 1, finptr);		//Write sample class
	fwrite(" ", sizeof(char), 1, finptr);		//Write sample class
	x = 0;
	fwrite(&x, sizeof(int), 1, finptr);		//Write sample class
	fwrite(".", sizeof(char), 1, finptr);		//Write sample class
	fwrite(&x, sizeof(int), 1, finptr);		//Write sample class
	fwrite(" ", sizeof(char), 1, finptr);		//Write sample class
	fwrite(title, sizeof(char), 30, finptr);	//Write sample class
	fclose(finptr);

	//
	FILE* foutptr = fopen("fobserved.dat", "w+b");
	char ctrltitle[31] = "Swag";
	Course ctrlcrs = {				//define control course based on what
		.division = "EN",			//output should be
		.dept = 0,
		.num = 100,
		.cred = {0, 0}
	};
	strcpy(ctrlcrs.title, ctrltitle);
	writeCourse(&ctrlcrs, foutptr);
	fclose(foutptr);
	assert(fileeq("fexpected.dat", "fobserved.dat"));
}

//needs to be done
void test_initDatabase(FILE* fptr) {
	
}

void test_courseList() {
	//Build input text file
	FILE* finptr = fopen("test.txt", "w");
	fprintf(finptr, "EN.070.725 2.0 Swegger 313\n");
	fprintf(finptr, "EN.007.364 1.0 Biggie is Ill\n");	//put out of order to check ability of 
	fprintf(finptr, "EN.700.899 3.0 Scrub Please\n");
	fclose(finptr);
	finptr = fopen("test.txt", "r");
	FILE* foutptr = fopen("test.dat", "w+b");
	courseList(finptr, foutptr);
	fclose(finptr);
	fclose(foutptr);
}

int main(void) {
	test_fileeq();
	test_parseString();
	test_parseShortString();
	test_writeCourse();
	test_courseList();
}
