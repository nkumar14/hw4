#include <stdio.h>
#include <string.h>
#define LENGTH 30
//header file for database manipulation functions

struct course{
	char division[3];
	//2 letter division code
	int dept, num;
	//3 digit number int codes for dept and coursenum
	int cred[2];
	//number of credits for course
	char title[31];
	//title of class following code string
};

typedef struct course Course;

//parse course text string into binary for database
Course* parseString(FILE* fptr);
//use code given by ta's on piazza

//parse given ddd.nnn string into 2 ints
int parseShortString(FILE* fptr, int retVal[]);
//simpler than TA code

//write course in necessary format to datafile
void writeCourse(Course* crs, FILE* foutptr);
//to be used throughout program

//initialize database with null values for later entry
void initDatabase(FILE* fptr);
//pass in pointer to file with w+b
//fwrite in repeat till full
//leave pointer open for use in other functions

//1. add list of courses from text file to database
void courseList(FILE* finptr, FILE* foutptr);
//get name of file from user
//order by course number
//if course number repeats, all repeats ignored and reported
//input errors (file, format errors) report as well
//process entire file, ignoring failed lines

//2. add single course to database
void singleCourse(FILE* foutptr);
//very similar to parse, except adds to database too
//must stay in course dept+num order

//3. display course information for given course
void dispCourse(FILE* fdatptr);
//use parseShortString
//find course, if doesn't exist print error
//else print all details of result

//4. display all records in database
void dispAll();
//prints all courses
//no fail case

//5. change name of a course
void changeName(char* crsnum, char* name);
//parseShortString, user inputs both crsnum and name
//if course doesn't exist or there is an input error, fail

//6. delete a course
void delCourse(char* crsnum);
//parseShortString
//if no course or error transaction, fail
//otherwise, record successful and replace with
//000.000 and blank strings

//7. add course to semester listing
void addToSem(int sem, char* crsnum, char* grade);
//user input semester, crsnum, and grade
//add into sem listing in dep.num order
//if sem isn't 0-9, course is invalid, letter grade is invalid,
//or course is already in sem list
//report fail

//8. remove course from sem listing
void delFromSem(int sem, char* crsnum);
//user input semester, crsnum
//delete crs from sem
//if sem isn't 0-9, course is invalid, course is not in list,
// report fail

//9. disp courses from semester
void dispSem(int sem);
//user input semester
//disp all
//fail if sem is invalid
//disp "No courses this semester" if no courses
//otherwise, display in correct format with grades at end of line

//10. create merged listing of courses in all semesters
void dispAllSem();
//merge all sems and display all courses
//ignore repeats
//use normal course format

//11. compute gpa for specific semester
void dispGPA(int sem);
//display gpa for specific semester
//user input semester
//semester is invalid, or contains no courses, fail
