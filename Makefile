# Decalre vars
CC = gcc
CFLAGS = -Wall -Wextra -pedantic -std=c99 -g $(GPROF)

#Add GPROF flags for GPROF tests
GPROF = 
progs = test_list test_database
#

test: test_list test_database
	@echo "Testing linked list functions..."
	./test_list
	@echo "Passed list tests"
	@echo "Testing database functions..."
	./test_database
	@echo "Passed all tests"

list.o: list.c list.h

test_list: test_list.c list.o

database.o: database.c database.h

test_database: test_database.c database.o

gproftest:

clean:
	rm -f *.o *.txt $(progs)
	
